<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# k8s-workshop

## Install required packages

- Docker Engine >= v20.10.5 (runc >= v1.0.0-rc93); recommended: 20.10.25
- k3d 5.5.2 (k3s version v1.27.4-k3s1)
- kubectl 1.26, 1.27; recommended: 1.27.4
- helm 3.12; recommended: 3.12.2

## Test your stack

1. Create a cluster.

    ```sh
    k3d cluster create
    ```

1. Get cluster info.

    ```sh
    kubectl cluster-info
    ```

1. Create Helm Chart.

    ```sh
    helm create hallowelt
    ```

1. Install the chart into the cluster.

    ```sh
    helm install hw hallowelt
    ```

1. Check if the application has been deployed successfully and runs on your local cluster.

    ```sh
    export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=hallowelt,app.kubernetes.io/instance=hw" -o jsonpath="{.items[0].metadata.name}")
    export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
    kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT
    ```

1. Check out the [service endpoint](http://127.0.0.1:8080) to see if everything is up and running (with a browser or any http client).

1. Stop the port forwarding (CTRL + C) and delete the cluster.

    ```sh
    k3d cluster delete
    ```

## Prepare your environment

1. [Kubectl autocomplete](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-autocomplete)
1. [Helm Completion](https://helm.sh/docs/helm/helm_completion/)
1. `export do='--dry-run=client -o yaml'`
1. Recreate the cluster with the setup required by the workshop.

    ```sh
    k3d cluster create \
    --registry-create registry:0.0.0.0:5000 \
    --port "8080:80@loadbalancer" \
    --agents 1
    ```

## License

This project aims to be [REUSE compliant](https://reuse.software/spec/).
Original parts are licensed under CC-BY-4.0.
Other assets are licensed under the respective license of the original.
Documentation, configuration and generated files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/hynek/k8s-workshop)](https://api.reuse.software/info/gitlab.gwdg.de/hynek/k8s-workshop)
